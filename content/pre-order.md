---
title: "Pre-Order Campaign"
---

# Subscribe Early to Prav

## Subscription Plans:

We are now opening up subscription options for Prav, you can now sign up at the following prices:

|         |  1 User |                                                   Subscribe                                                   |
|:-------:|:-------:|:-------------------------------------------------------------------------------------------------------------:|
| 1 User  |   ₹200  |        <a herf="https://rzp.io/i/OSQ2mcvpSc"><button class="button">Subscribe For Yourself</button></a>       |
| 3 Users |   ₹500  | <a herf="https://rzp.io/i/xqzp5TUMBp"><button class="button">Subscribe With Your Group Or Family</button></a> |

These can be purchased now and redeemed when Prav matures and is ready for everyday usage.

## What Subscription?
Currently Prav is maintained by volunteer Developers and System Administrators, which has helped push the app to its current stage, but this isn't feasible for long term development, to open Prav to the public, we cannot hope to rely only upon volunteers, by buying a subscription today, you can help fund the hriing of people to actively maintain Prav by allowing stable and active work on it,

## Why Pre-Order?
By Pre-Ordering now, You save on the costs of subscription, as these prices will increase after a public release, allowing you cheaper access to the service.

## How do I subscribe?
We will be working with Navodaya who will act as the project's fiscal host until the Co-operative is set up, you will be directed to a Razorpay system set up with them, for people outside of India, you can use the [open collective campaign](https://opencollective.com/prav-ios).

## Help out Prav
Help out Prav's development by purchasing a subscription for yourself and others today, you can also help by donating money to the project that would help forward the project!

## Pre-Order Now!
We are using Razorpay Subscriptions for payment processing, you can find more about that [here](https://razorpay.com/docs/payments/subscriptions/).

### Discounted Long Term Plans
(Available via UPI Payments Only)

<a herf="https://rzp.io/i/kJOYvcWwQc"><button class="button">Save ₹100 for yourself by subscribing for 3 years</button></a>

<a herf="https://rzp.io/i/YHVJ6NF8dI"><button class="button">Save ₹300 if you subscribe for 3 years along with your family or group</button></a>.

*Currently we only offer discounted subscriptions for UPI Payments, Discounts via Credit and Debit Cards would involve creating offers for all banks*
