---
title: "Homepage"
meta_title: "Prav App"
description: "Prav Messaging App- Privacy and convenience with choice of providers"
intro_image: "/images/prav-screenshot.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

# Prav Messaging App- Privacy and convenience with choice of providers

Prav is a messaging service which can be used to exchange messages, audio/video calls, files, images and videos through internet. Prav App is convenient without [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in). 

<p>Many mainstream messaging apps allow you to talk only to people who use the same app, other apps which provide choice have many manual steps and people complain they are inconvenient.</p>

<p>Inspired by Quicksy app, Prav provides both convenience of entering only a phone number and at the same time allow talking to people using any app that understands XMPP protocol like Quicksy, Monocles Chat, Dino, Gajim or Monal.</p>
<p>
  <a href="/about"><button class="button">Learn More</button></a>
  <a href="/volunteer" class="button">Get involved</a>
</p>

<p>We are currently in private beta phase and planning for a <a href="https://prav.app/blog/inviting-developers-for-prav-public-beta/">public beta release of Prav App</a>.


<style>
.button
{
background-color: #ff1493;
border:none;
margin: 0.2rem 0;
}
<div>
</style>

Currently, prav.app has 275 registered users.

<p>
  <a href="https://f-droid.org/en/packages/app.prav.client"><button class="button">Try Prav App Beta on Android</button></a>
</p>
