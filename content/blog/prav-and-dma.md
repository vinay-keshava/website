---
title: "Prav and the Digital Markets Act by European Union"
date: 2022-04-26T17:12:18+05:30
draft: false
---
We at Prav welcome the draft of the [Digital Markets Act by the European Commission](https://ec.europa.eu/info/strategy/priorities-2019-2024/europe-fit-digital-age/digital-markets-act-ensuring-fair-and-open-digital-marketsen), which attempts to dilute the gatekeeping power of big companies by forcing them to open the market to new players, but we believe there is much room for improvement. For example, Apple will be made to allow iPhone users to download apps from outside their App Store, and WhatsApp will have to let its users to communicate with others through alternative messengers.

### Room for improvement 

Agreeing with the [Electronic Frontier Foundation](https://www.eff.org/deeplinks/2020/12/eus-digital-markets-act-there-lot-room-improvement), there could have been a better version of the DMA that resonates with ethical values. It's unfortunate that there is no prohibition of collection of private data in the first place, as per the DMA. 

### What about India? 

In India, we are not obliged to follow the DMA as of now, but the Prav service will be compatible with DMA, also the DMA isn't expected to come to force before 2023-24. We will allow our users to [talk to other XMPP services](/about/#social-contract). With the current technology, public groups created on Prav service [will be interoperable](https://fsci.in/blog/bridging-all-the-messengers/) with matrix, IRC, telegram. In addition, our service will provide users [software freedom](https://www.gnu.org/philosophy/free-software-even-more-important.html), along with control over the privacy policy of the Prav service.

We need to raise awareness in India for privacy and freedom. India needs such laws as well. Till then, you can use XMPP and invite others to use it by installing Quicksy app or Prav app(when it gets released) and enjoy the freedoms provided by the DMA.
